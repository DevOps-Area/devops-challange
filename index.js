const keys = require('./keys');

// Express App Setup
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());

// Postgres Client Setup
const { Pool } = require('pg');
const pgClient = new Pool({
  user: keys.pgUser,
  host: keys.pgHost,
  database: keys.pgDatabase,
  password: keys.pgPassword,
  port: keys.pgPort
});
pgClient.on('error', () => console.log('Lost PG connection'));

pgClient
  .query('CREATE TABLE IF NOT EXISTS values (number INT, square INT)')
  .catch(err => console.log(err));


// Express route handlers

app.get('/', (req, res) => {
  res.send('Hi');
});

app.get('/values/all', async (req, res) => {
  console.log ('get all');
  const values = await pgClient.query('SELECT * from values');
  res.send(values.rows);
});

app.get('/values/current', async (req, res) => {
  const values = await pgClient.query('SELECT number from values')
    res.send(values.rows);
  });

app.post('/values', async (req, res) => {
  const index = parseInt(req.body.index);

  // it was index[0], 123 -> 1
  // parseInt(index[0]**2) -> it should be parsed first as Integer
  const squared = index**2;
    pgClient.query('INSERT INTO values(number, square) VALUES($1, $2)', [index, squared])
    .then(result => { // it was res as the main request's response object

      // there was no res.send, which makes the request in pending state
      res.send();
    })
    .catch(e => console.error(e.stack));
});

app.listen(5000, err => {
  console.log('Listening');
});
